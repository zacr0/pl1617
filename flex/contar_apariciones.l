/*
* Ejercicio 4
*
* @file 	contar_apariciones.l
* @author 	Pablo Medina Suarez
*
* Programa que recibe un fichero de texto y una palabra para contar el numero
* de veces que aparece dicha palabra en el fichero.
*
* NOTA: se ha interpretado el concepto de igualdad como el de que ambas palabras
* contengan los mismos caracteres, independientemente de si hay diferencia en
* las mayusculas o no.
* 
* Por tanto, las palabras "test", TEST" y "tEsT" seran consideradas como iguales.
*
*/

/* Zona de definiciones */
%{
	#include <ctype.h>
	#include <string.h>

	char *palabra;	// palabra buscada en el fichero de entrada
	unsigned int num_apariciones = 0;	// numero de veces que aparece palabra

	/**
	* @brief	Convierte los caracteres de una cadena a sus equivalentes
	* en minuscula.
	* @param[in] str Cadena convertida a minusculas.
	*/
	void cadena_a_minusculas(char *str);

	/**
	* @brief	Determina si dos cadenas son iguales, con insensibilidad 
	* a mayusculas.
	* @param[in] str_a 	Primera cadena a comparar.
	* @param[in] str_b 	Segunda cadena a comparar
	* @return 1 si str_a = str_b, 0 de lo contrario.
	*/
	int comparar_insensible_mayus(char *str_a, char *str_b);
%}

palabra 	[^ \t\n]+

/* Zona de reglas */
%%
{palabra} {
	if (comparar_insensible_mayus(yytext, palabra))	{
		num_apariciones++;
	}
}

.|\n 	;

%%
/* Zona de codigo */
extern FILE *yyin;
int main(int argc, char** argv) {
	// Control de numero de argumentos
	if (argc == 3) {
		yyin = fopen(argv[1], "r");
		if (yyin == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
			return -1;
		}
		
		// Reserva de cadena
		palabra = argv[2];
	} else {
		printf("Formato incorrecto. Formato: ./contar_apariciones.exe <entrada.txt> palabra_a_buscar\n");
		return -1;
	}

	// Analisis de fichero
	yylex();

	// Cierre de fichero
	fclose(yyin);

	// Resultados del conteo tras la lectura
	fprintf(stdout, "- La palabra \"%s\" aparece %d veces.\n", palabra, num_apariciones);

	return 0;
} // main

// Funciones adicionales
void cadena_a_minusculas(char *str) {
	for (int i = 0; str[i]; i++) {
		str[i] = tolower(str[i]);
	}
} // cadena_a_minusculas

int comparar_insensible_mayus(char *str_a, char *str_b) {
	int son_iguales = 0;
	char *str_a_minus, *str_b_minus;

	// Reserva de memoria para copias en minuscula
	str_a_minus = (char *) malloc(strlen(str_a)+1);
	strcpy(str_a_minus, str_a);
	cadena_a_minusculas(str_a_minus);

	str_b_minus = (char *) malloc(strlen(str_b)+1);
	strcpy(str_b_minus, str_b);
	cadena_a_minusculas(str_b_minus);

	// Comparacion de cadenas
	if (strcmp(str_a_minus, str_b_minus) == 0) {
		son_iguales = 1;
	}

	// Liberacion de memoria
	free(str_a_minus);
	free(str_b_minus);

	return son_iguales;
} // comparar_insensible_mayus
