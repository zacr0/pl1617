/*
* Ejercicio 6
*
* @file 	registro_accesos.l
* @author 	Pablo Medina Suarez
*
* Analizador lexico que permite:
*  - Contar cuantos acceso ha realizado un usuario
* 		- Argumentos: fichero de registro, nombre de usuario
*  - Contar cuantos accesos ha realizado un usuario en un dia concreto.
*		- Argumentos: fichero de registro, nombre de usuario, dia a consultar
*
* El fichero tiene la siguiente informacion en cada linea:
* 	usuario fecha hora
* 
* Un usuario puede tener letras, numeros y '_'
* Las fechas tienen el formato AAAA-MM-DD, '-' es intercambiable con '/'
* Las horas tienen el formato HH:MM:SS
*/

/* Zona de definiciones */
%{
	// Variables
	unsigned int num_accesos = 0,
				 busqueda_fecha = 0; // booleano, = 1 -> se busca dia concreto
	char *usuario,
		 *dia_consultado;
%}

/* Definiciones regulares */
identificador	[a-zA-Z0-9_]+
digito	[0-9]
fecha	{digito}{4}(\-|\/){digito}{2}(\-|\/){digito}{2}
hora	({digito}{2}:){2}{digito}{2}

/* Estado: busqueda por fecha */
%x ESTADO_USUARIO_FECHA 

/* Zona de reglas */
%%

{identificador} {
	if (strcmp(yytext, usuario) == 0) {
		if (busqueda_fecha) {
			// No se contabiliza el acceso, se espera coincidir con la fecha
			BEGIN ESTADO_USUARIO_FECHA;
		} else {
			// No se busca por fecha, se contabiliza el acceso
			num_accesos++;
		}
	}
}

<ESTADO_USUARIO_FECHA>{fecha} {
	if (strcmp(yytext, dia_consultado) == 0) {
		// Fecha coincidente, se contabiliza el acceso
		num_accesos++;
	}
	// Retorna al estado por defecto
	BEGIN(INITIAL);
}

.|\n ;

%%
/* Zona de codigo */
extern FILE *yyin;
int main(int argc, char** argv) {

	// Control de numero de argumentos
	switch(argc) {
		case 3: 
			// Contar accesos: fichero_registro nombre_usuario
			break;
		case 4:
			// Contar accesos en un dia: fichero_registro nombre_usuario fecha
			dia_consultado = argv[3];

			// Activa flag de busqueda por fecha
			busqueda_fecha = 1;

			break;
		default:
			printf("- Formato incorrecto. Formato: ./registro_accesos.exe fich_registro nombre_usuario [AAAA-MM-DD]\n");
			return -1;	
	}

	// Nombre de usuario (comun para ambos casos)
	usuario = argv[2];

	// Apertura de ficheros
	yyin = fopen(argv[1], "r");
	if (yyin == NULL) {
		fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
		return -1;
	}

	// Analisis del fichero
	yylex();

	// Cierre de fichero de datos
	fclose(yyin);

	// Resultados
	if (busqueda_fecha) {
		fprintf(stdout, "- El usuario \"%s\" accedio %d veces el dia \"%s\".\n", usuario, num_accesos, dia_consultado);
	} else {
		fprintf(stdout, "- El usuario \"%s\" ha accedido %d veces en total.\n", usuario, num_accesos);
	}

	return 0;
} // main
