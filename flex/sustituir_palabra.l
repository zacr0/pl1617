/*
* Ejercicio 5
*
* @file 	sustituir_palabra.l
* @author 	Pablo Medina Suarez
*
* Reemplaza una palabra por otra en un fichero de entrada.
*
* Ambas palabras, asi como el nombre del fichero deberan ser introducidos
* por el usuario, bien a traves de la linea de comandos o cuando el usuario
* ejecute el programa.
*
*/

/* Zona de definiciones */
%{
	#include <ctype.h>
	#include <string.h>

	// Variables
	char *palabra_reemplazada;	// palabra buscada en el fichero de entrada
	char *nueva_palabra;		// palabra que reemplaza a palabra_reemplazada

	/**
	* @brief	Convierte los caracteres de una cadena a sus equivalentes
	* en minuscula.
	* @param[in] str Cadena convertida a minusculas.
	*/
	void cadena_a_minusculas(char *str);

	/**
	* @brief	Determina si dos cadenas son iguales, con insensibilidad 
	* a mayusculas.
	* @param[in] str_a 	Primera cadena a comparar.
	* @param[in] str_b 	Segunda cadena a comparar
	* @return 1 si str_a = str_b, 0 de lo contrario.
	*/
	int comparar_insensible_mayus(char *str_a, char *str_b);

	/**
	* @brief 	Lee una cadea por teclado, limpiando \n.
	* @param[in] str 	Cadena leida
	* @param[in] maximo	Longitud maxima aceptada por la cadena.
	*/
	void leer_cadena(char *str, int maximo);
%}

palabra 	[^ \t\n]+

/* Zona de reglas */
%%
{palabra} {
	if (comparar_insensible_mayus(yytext, palabra_reemplazada))	{
		// Se ha encontrado la palabra buscada, se reemplaza
		fprintf(yyout, "%s", nueva_palabra);
	} else {
		// Se escribe el texto leido sin modificar
		fprintf(yyout, "%s", yytext);
	}
}

.|\n {
	// Cualquier otro caracter se escribe tal cual
	fprintf(yyout, "%s", yytext);
}

%%
/* Zona de codigo */
extern FILE *yyin, *yyout;
int main(int argc, char** argv) {
	int tam_buffer = 256;
	char aux_reemp[tam_buffer],
		 aux_nueva[tam_buffer],
		 ruta_org[tam_buffer],
		 ruta_tmp[] = "tmp.txt";

	// Control de numero de argumentos
	switch(argc) {
		case 1:
			// Lectura de parametros por teclado
			puts("-- Introduzca la ruta del fichero: ");
			leer_cadena(ruta_org, tam_buffer);

			puts("-- Introduzca la palabra a reemplazar: ");
			leer_cadena(aux_reemp, tam_buffer);
			palabra_reemplazada = aux_reemp;

			puts("-- Introduzca la palabra por la que se sustituye:");
			leer_cadena(aux_nueva, tam_buffer);
			nueva_palabra = aux_nueva;

			break;
		case 4:
			// Fichero de origen de datos
			strcpy(ruta_org, argv[1]);
			//ruta_org = argv[1];

			// Asignacion de palabras
			palabra_reemplazada = argv[2];
			nueva_palabra = argv[3];

			break;
		default:
			printf("- Formato incorrecto. Formato: ./sustituir_palabra.exe <entrada.txt> palabra_reemplazada nueva_palabra\n");
			return -1;	
	}

	// Apertura de ficheros
	yyin = fopen(ruta_org, "r");
	if (yyin == NULL) {
		fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", ruta_org);
		return -1;
	}
	// Fichero temporal con las sustituciones realizadas al original
	yyout = fopen(ruta_tmp, "w");
	if (yyout == NULL) {
		fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", ruta_tmp);
		return -1;
	}

	// Analisis del fichero
	yylex();

	// Sustitucion de fichero original por fichero temporal modificado
	fclose(yyin);
	fclose(yyout);

	// Renombra el fichero de datos temporal con el nombre del original
	if (rename(ruta_tmp, ruta_org) != 0) {
		fprintf(stdout, "- Error al reemplazar archivo de datos.\n");
	}

	return 0;
} // main

// Funciones adicionales
void cadena_a_minusculas(char *str) {
	for (int i = 0; str[i]; i++) {
		str[i] = tolower(str[i]);
	}
} // cadena_a_minusculas

int comparar_insensible_mayus(char *str_a, char *str_b) {
	int son_iguales = 0;
	char *str_a_minus, *str_b_minus;

	// Reserva de memoria para copias en minuscula
	str_a_minus = (char *) malloc(strlen(str_a)+1);
	strcpy(str_a_minus, str_a);
	cadena_a_minusculas(str_a_minus);

	str_b_minus = (char *) malloc(strlen(str_b)+1);
	strcpy(str_b_minus, str_b);
	cadena_a_minusculas(str_b_minus);

	// Comparacion de cadenas
	if (strcmp(str_a_minus, str_b_minus) == 0) {
		son_iguales = 1;
	}

	// Liberacion de memoria
	free(str_a_minus);
	free(str_b_minus);

	return son_iguales;
} // comparar_insensible_mayus

void leer_cadena(char *str, int maximo) {
	fgets(str, maximo, stdin);

	if (str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = '\0';
	}
} // leer_cadena