/*
* Ejercicio 3
*
* @file 	contar.l
* @author 	Pablo Medina Suarez
*
* Elaborar un programa de flex que reciba un fichero de texto y cuente 
* el numero de caracteres, palabras y lineas que contiene.
*
*/

/* Zona de definiciones */
%{
	unsigned int num_caracteres = 0,
				 num_palabras = 0,
				 num_lineas = 1;
%}

caracter 	.|\n
palabra 	[^ \t\n]+
linea 		\n

/* Zona de reglas */
%%

{linea} { 
	num_lineas++;
	num_caracteres++;
	REJECT;
}

{palabra} {
	num_palabras++;
	num_caracteres += yyleng; 
}

{caracter} {
	num_caracteres++;
}

.|\n 	;

%%
/* Zona de codigo */
extern FILE *yyin;
int main(int argc, char** argv) {
	// Control de numero de argumentos
	if (argc == 2) {
		yyin = fopen(argv[1], "r");
		if (yyin == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
			return -1;
		}
	} else {
		printf("Formato incorrecto. Formato: ./contar.exe <entrada.txt>\n");
		return -1;
	}

	// Analisis de fichero
	yylex();

	// Cierre de fichero
	fclose(yyin);

	// Resultados del conteo tras la lectura
	fprintf(stdout, "- Caracteres: %d", num_caracteres);
	fprintf(stdout, "\n- Palabras: %d", num_palabras);
	fprintf(stdout, "\n- Lineas: %d\n", num_lineas);

	return 0;
} // main
