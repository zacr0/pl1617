# Fichero original sin errores

(*
	En este fichero se se muestra un ejemplo de pseudocodigo bien
	formado y sin errores para demostrar el funcionamiento del programa
	desarrollado (analizador_pseudocodigo.exe)

	@author Pablo Medina Suarez
*)

# Declaracion de variables
a := 10;
letra_a := 'a';
cadena_de_caracteres := 'hola que tal, cadena con \'comillas\'';
numero_notacion_cientica := 1e-20;
num_decimal := 5.9999;

# Operaciones
a := a * 2;
num_decimal := num_decimal + a - numero_notacion_cientica;
resto := a __mod 2;
resto := resto ** 2;

# Comparaciones
si ((resto > __no a) __o (a <> numero_notacion_cientica)) entonces
	cadena_de_caracteres := 'otra \'cadena\' de caracteres';
SI_NO
	cadena_de_caracteres := letra_a;
fin_si

mientras (1) hacer
	escribir('verdadero')
	leer(cadena_de_caracteres)
FIN_MIENTRAS