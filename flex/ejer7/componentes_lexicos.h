#ifndef COMPONENTES_LEXICOS_H
#define COMPONENTES_LEXICOS_H

// Delimitadores de bloque
#define INICIO 		257
#define FIN 		258

// Operadores logicos
#define __O 		259
#define __Y 		260
#define __NO 		261

// Acciones
#define LEER 		262
#define ESCRIBIR 	263

// Control de flujo
#define SI 			264
#define ENTONCES 	265
#define SI_NO 		266
#define FIN_SI 		267
#define MIENTRAS 	268
#define HACER 		269
#define FIN_MIENTRAS 270
#define REPETIR 	271
#define HASTA_QUE 	272
#define PARA 		273
#define DESDE 		274
#define HASTA 		275
#define PASO 		276
#define FIN_PARA 	277

// Identificador
#define IDENTIFICADOR 278

// Numero
#define NUMERO 		279

// Cadena
#define CADENA 		280

// Operador de asignacion
#define ASIGNACION 	281

// Operadores arimeticos
#define SUMA 		282
#define RESTA 		283
#define PRODUCTO 	284
#define DIVISION	285
//#define MODULO		287
#define __MOD 		286
#define POTENCIA	287

// Operador alfanumerico
#define CONCATENACION 288

// Operadores relacionales de numeros y cadenas
#define MENOR_QUE	289
#define MENOR_IGUAL_QUE 290
#define MAYOR_QUE	291
#define MAYOR_IGUAL_QUE 292
#define IGUAL 		293
#define DISTINTO	294

// Comentarios
#define COMENTARIO_LINEA 295
#define COMENTARIO_MULTILINEA 296

// Otros componentes
#define FIN_SENTENCIA 297
#define PARENTESIS_IZQ 298
#define PARENTESIS_DER 299

#endif
