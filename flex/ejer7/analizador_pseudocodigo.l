/*
* Ejercicio 7
*
* @file 	analizador_pseudocodigo.l
* @author 	Pablo Medina Suarez
*
* Analizador lexico que permita reconocer los componentes lexicos de un 
* programa escrito en pseudocodigo.
*
* Para una definicion mas extensa consultar el enunciado del ejercicio.
*
* Identificacion de componentes lexicos por colores:
*  - Rojo (\e[31m): errores
*  - Verde (\e[92m): palabras reservadas
*  - Azul (\e[94m): operadores
*  - Gris (\e[37m): comentarios
*  - Magenta (\e[95m): numeros e identificadores
*  - Cyan (\e[36m): cadenas
*/

/* Zona de definiciones */
%{
	#include "componentes_lexicos.h" // tokens
	unsigned int num_errores = 0,
				 num_linea = 1,
	             linea_comentario = 0,
	             linea_cadena = 0;
%}

/* Definiciones regulares */
/* Palabras reservadas */
inicio		(?i:inicio)
fin 		(?i:fin)
__mod		__(?i:mod)
__o 		__(?i:o)
__y			__(?i:y)
__no		__(?i:no)
leer		(?i:leer)
escribir 	(?i:escribir)
si			(?i:si)
entonces 	(?i:entonces)
si_no		(?i:si_no)
fin_si 		(?i:fin_si)
mientras	(?i:mientras)
hacer 		(?i:hacer)
fin_mientras (?i:fin_mientras)
repetir 	(?i:repetir)
hasta_que 	(?i:hasta_que)
para 		(?i:para)
desde 		(?i:desde)
hasta 		(?i:hasta)
paso 		(?i:paso)
fin_para 	(?i:fin_para)

/* Identificadores, numeros, cadenas */
letra 		[a-zA-Z]
digito 		[0-9]
numero 		[\-]?{digito}+(\.{digito}+)?((?i:e)[+\-]?{digito}+)?
caracter 	[a-zA-Z0-9]
identificador 	{letra}({caracter}|(_{caracter}))*{caracter}*

/* Otros */
espacio	[ \t]

/* Estados */
%x ESTADO_CADENA
%x ESTADO_COMENTARIO

/* Zona de reglas */
%%
{espacio} { ; }
\n {
	num_linea++;
}

{inicio}	{ fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, INICIO); }
{fin}	{ fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, FIN); }

{__mod} {
	fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, __MOD);
	fprintf(yyout, "\e[94mOperador relacional:\e[0m aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, __MOD);
}
{__o} {
	fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, __O);
	fprintf(yyout, "\e[94mOperador relacional:\e[0m logico:\e[0m <%s> --> token <%d>.\n", yytext, __O);
}
{__y} {
	fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, __Y);
	fprintf(yyout, "\e[94mOperador relacional:\e[0m logico:\e[0m <%s> --> token <%d>.\n", yytext, __Y);
}
{__no} {
	fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, __NO);
	fprintf(yyout, "\e[94mOperador relacional:\e[0m logico:\e[0m <%s> --> token <%d>.\n", yytext, __NO);
}

{leer} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, LEER); }
{escribir} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, ESCRIBIR); }

{si} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, SI); }
{entonces} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, ENTONCES); }
{si_no} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, SI_NO); }
{fin_si} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, FIN_SI); }

{mientras} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, MIENTRAS); }
{hacer} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, HACER); }
{fin_mientras} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, FIN_MIENTRAS); }

{repetir} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, REPETIR); }
{hasta_que} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, HASTA_QUE); }

{para} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, PARA); }
{desde} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, DESDE); }
{hasta} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, HASTA); }
{paso} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, PASO); }
{fin_para} { fprintf(yyout, "\e[92mPalabra reservada:\e[0m <%s> --> token <%d>.\n", yytext, FIN_PARA); }

":=" { fprintf(yyout, "\e[94mOperador de asignacion:\e[0m <%s> --> token <%d>.\n", yytext, ASIGNACION); }
"+" { fprintf(yyout, "\e[94mOperador aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, SUMA); }
"-" { fprintf(yyout, "\e[94mOperador aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, RESTA); }
"*" { fprintf(yyout, "\e[94mOperador aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, PRODUCTO); }
"/" { fprintf(yyout, "\e[94mOperador aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, DIVISION); }
"**" { fprintf(yyout, "\e[94mOperador aritmetico:\e[0m <%s> --> token <%d>.\n", yytext, POTENCIA); }
"||" { fprintf(yyout, "\e[94mOperador alfanumerico:\e[0m <%s> --> token <%d>.\n", yytext, CONCATENACION); }

"<" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, MENOR_QUE); }
"<=" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, MENOR_IGUAL_QUE); }
">" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, MAYOR_QUE); }
">=" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, MAYOR_IGUAL_QUE); }
"==" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, IGUAL); }
"<>" { fprintf(yyout, "\e[94mOperador relacional:\e[0m <%s> --> token <%d>.\n", yytext, DISTINTO); }

";" { fprintf(yyout, "Fin de sentencia: <%s> --> token <%d>.\n", yytext, FIN_SENTENCIA); }
"(" { fprintf(yyout, "Parentesis izquierdo: <%s> --> token <%d>.\n", yytext, PARENTESIS_IZQ); }
")" { fprintf(yyout, "Parentesis derecho: <%s> --> token <%d>.\n", yytext, PARENTESIS_DER); }

"#".* { fprintf(yyout, "\e[37mComentario de una linea: <%s>\e[0m --> token <%d>.\n", yytext, COMENTARIO_LINEA); }
"*)" {
	// Cierre de comentario sin haberse abierto
	fprintf(yyout, "\e[31mERROR: cierre de comentario inesperado en la linea <%d>.\e[0m\n", num_linea);
	num_errores++;

	// Finaliza el analisis
	//yyterminate();
}
"(*" { 
	// Comienzo de comentario de varias lineas
	BEGIN ESTADO_COMENTARIO;

	// Apunta la linea de inicio del comentario (posible indicacion de errores)
	linea_comentario = num_linea;
}
<ESTADO_COMENTARIO>"(*" {
	// Comentario mal formado
	fprintf(yyout, "\e[31mERROR: comentario mal formado en la linea <%d>.\e[0m\n", num_linea);
	num_errores++;

	// Finaliza el analisis
	yyterminate();
}
<ESTADO_COMENTARIO>"*)" { 
	// Fin del comentario de multiples lineas bien formado
	// Elimina los caracteres *) del texto
	yytext[yyleng - 2] = '\0';

	fprintf(yyout, "\e[37mComentario de varias lineas: <%s>\e[0m --> token <%d>\n", yytext, COMENTARIO_MULTILINEA);

	// Retorna al estado por defecto
	BEGIN(INITIAL);
}
<ESTADO_COMENTARIO>. { yymore(); }
<ESTADO_COMENTARIO>\n {
	yymore(); 
	num_linea++;
}
<ESTADO_COMENTARIO><<EOF>> {
	// Comentario sin cerrar
	fprintf(yyout, "\e[31mERROR: comentario de la linea <%d> sin cerrar.\e[0m\n", linea_comentario);
	num_errores++;

	// Finaliza el analisis
	yyterminate();
}

"'" { 
	// Comienzo de una cadena de texto
	BEGIN ESTADO_CADENA;

	// Apunta la linea de inicio de la cadena (posible indicacion de errores)
	linea_cadena = num_linea;
}
<ESTADO_CADENA>"'" {
	// Fin de cadena
	// Elimina la ultima '
	yytext[yyleng - 1] = '\0';
	
	fprintf(yyout, "\e[36mCadena: <%s>\e[0m --> token <%d>.\n", yytext, CADENA);

	// Retorna al estado inicial
	BEGIN(INITIAL);
}
<ESTADO_CADENA><<EOF>> {
	// Cadena sin cerrar
	fprintf(yyout, "\e[31mERROR: cadena de la linea <%d> sin cerrar.\e[0m\n", linea_cadena);
	num_errores++;

	// Finaliza el analisis
	yyterminate();
}
<ESTADO_CADENA>"\\\'" { yymore(); }
<ESTADO_CADENA>\n {
	yymore();
	num_linea++;
}
<ESTADO_CADENA>. { yymore(); }

{identificador} { fprintf(yyout, "\e[95mIdentificador:\e[0m <%s> --> token <%d>.\n", yytext, IDENTIFICADOR); }

{numero} { fprintf(yyout, "\e[95mNumero:\e[0m <%s> --> token <%d>.\n", yytext, NUMERO); }

. { 
	// Componente desconocido (no ha sido captado por las anteriores)
	fprintf(yyout, "\e[31mERROR: componente desconocido <%s> en la linea <%d>.\e[0m\n", yytext, num_linea);
	num_errores++;
}

%%
/* Zona de codigo */
extern FILE *yyin, *yyout;
int main(int argc, char** argv) {

	// Control de numero de argumentos
	switch(argc) {
		case 2:
			// Se indica fichero de entrada de datos
			break;
		case 3:
			// Se indica fichero de salida de datos
			yyout = fopen(argv[2], "w");
			if (yyout == NULL) {
				fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[2]);
				return -1;
			}

			break;
		default:
			printf("- Formato incorrecto. Formato: ./analizador_pseudocodigo.exe fichero_codigo [fichero_salida]\n");
			return -1;	
			
	}

	// Apertura de fichero de entrada
	yyin = fopen(argv[1], "r");
	if (yyin == NULL) {
		fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
		return -1;
	}

	// Analisis del fichero
	yylex();

	// Cierre de fichero de datos
	fclose(yyin);

	// Resultados
	if (num_errores) {
		fprintf(yyout, "- \e[31mAnalisis finalizado. Se han encontrado %d error(es). Revise la salida.\e[0m\n", num_errores);
	} else {
		fprintf(yyout, "- \e[92mAnalisis finalizado. No se encontraron errores.\e[0m\n");
	}

	return 0;
} // main

