/*
* Ejercicio 1
*
* @file 	sumar.l
* @author 	Pablo Medina Suarez
*
* Codifica un programa en flex que copie el archivo de entrada en uno de
* salida, poniendo a 0 todo numero positivo que sea multiplo de 3.
*/

/* Zona de definiciones */
digito	[0-9]+

/* Zona de reglas */
%%
{digito} {
	if (atoi(yytext) % 3 == 0) {
		// Es multiplo de 3
		fprintf(yyout, "0");
	} else {
		fprintf(yyout, "%s", yytext);
	}
}

.|\n ;

%%
/* Codigo de usuario */
extern FILE *yyin, *yyout;
int main(int argc, char** argv) {
	// Control de numero de argumentos
	if (argc == 3) {
		yyin = fopen(argv[1], "r");
		if (yyin == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
			return -1;
		}

		yyout = fopen(argv[2], "w");
		if (yyout == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[2]);
			return -1;
		}
	} else {
		printf("Formato incorrecto. Formato: ./sumar.exe <entrada.txt> <salida.txt>\n");
		return -1;
	}

	// Analisis de fichero
	yylex();

	// Cierre de ficheros
	fclose(yyin);
	fclose(yyout);

	return 0;
} // main
