/*
* Ejercicio 2
*
* @file 	sumar_parametrizado.l
* @author 	Pablo Medina Suarez
*
* Programa que copia el archivo de entrada en uno de salida, sumando n1 a todo
* numero positivo que sea multiplo de n2, donde n1 y n2 son dos numeros pasados
* como argumentos desde la linea de ordenes.
*/

/* Zona de definiciones */
%{
	#include <math.h> 	// compilar con -lm (usar makefile)

	float num1,	// numero sumado a todo multiplo de num2
		  num2,	// numero del cual deben ser multiplos los digitos leidos
		  valor;	// caracteres leidos como flotantes

%}

digito 	[0-9]
numero 	[\-]?{digito}+(\.{digito}+)?((?i:e)[+\-]?{digito}+)?

/* Zona de reglas */
%%
{numero} {
	valor = atof(yytext);

	// Se filtran los numeros positivos
	if ( (valor > 0) && (fmod(valor, num2) == 0.0) ) {
		// Es positivo y multiplo de num2, se suma num1
		valor += num1;
		fprintf(yyout, "%lf", valor);
	} else {
		// No se trata el numero
		fprintf(yyout, "%s", yytext);
	}
}

.|\n ;


%%
/* Codigo de usuario */
extern FILE *yyin, *yyout;
int main(int argc, char** argv) {
	// Control de numero de argumentos
	if (argc == 5) {
		yyin = fopen(argv[1], "r");
		if (yyin == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[1]);
			return -1;
		}

		yyout = fopen(argv[2], "w");
		if (yyout == NULL) {
			fprintf(stdout, "- Error al abrir el fichero \"%s\".\n", argv[2]);
			return -1;
		}

		num1 = atof(argv[3]);
		num2 = atof(argv[4]);
	} else {
		printf("Formato incorrecto. Formato: ./sumar_parametrizado.exe <entrada.txt> <salida.txt> numero1 numero2\n");
		return -1;
		
	}

	// Analisis de fichero
	yylex();

	// Cierre de ficheros
	fclose(yyin);
	fclose(yyout);

	return 0;
} // main
