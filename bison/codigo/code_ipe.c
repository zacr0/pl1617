#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ipe.h"
#include "ipe.tab.h"

#include "macros.h"

#define NSTACK 256              /* Dimension maxima de la pila */
static Datum stack[NSTACK];     /* La pila */
static Datum *stackp;           /* siguiente lugar libre en la pila */

#define NPROG 2000
Inst prog[NPROG];  /* La maquina */
Inst *progp;       /* Siguiente lugar libre para la generacion de codigo */

Inst *pc; /* Contador de programa durante la ejecucion */

void initcode() {
    /* inicializacion para la generacion de codigo */
    stackp = stack;
    progp = prog;
}

void push(Datum d) {
    /* meter dato d en la pila */

    /* Comprobar que hay espacio en la pila para el nuevo valor o variable */
    if (stackp >= &stack[NSTACK]) {
        execerror (" Desborde superior de la pila ", (char *) 0);
    }

    *stackp++ = d; /* Apilar la variable o el numero y */
   /* desplazar el puntero actual de la pila */
}

Datum pop() {
    /* sacar y devolver de la pila el elemento de la cima */

    /* Comprobar que no se intenta leer fuera de la pila */
    /* En teoria no ocurrira nunca */
    if (stackp <= stack) {
        execerror (" Desborde inferior de la pila ", (char *) 0);
    }

    --stackp;          /* Volver hacia atras una posicion en la pila */
    return(*stackp);   /* Devolver variable o numero */
}

void pop2() {
    /* sacar y  NO devolver el elemento de la cima de la pila */

    /* Comprobar que no se intenta leer fuera de la pila */
    /* En teoria no ocurrira nunca */

    if (stackp <= stack) {
        execerror (" Desborde inferior de la pila ", (char *) 0);
    }

    --stackp; /* Volver hacia atras una posicion en la pila */
}

Inst *code(Inst f) {
    /* Instalar una instruccion u operando */
    Inst *oprogp = progp;   /* Puntero auxiliar */

    /* Comprobar que hay espacio en el vector de instrucciones */

    if (progp >= &prog[NPROG]) {
        execerror (" Programa demasiado grande", (char *) 0);
    }

    *progp=f;        /* Asignar la instruccion o el puntero a la estructura */
    progp++;         /* Desplazar una posicion hacia adelante */
    return (oprogp);
}

void execute(Inst *p) {
    /* Ejecucion con la maquina */

    /* El contador de programa pc se inicializa con la primera instruccion a */
    /* ejecutar */
    for (pc=p; *pc != STOP;   ) {
        /* Ejecucion de la instruccion y desplazar */
        /* el contador de programa pc */
        (*(*pc++))();
    }
}

/****************************************************************************/
/****************************************************************************/
void assign() {
    // asigna el valor superior al siguiente valor
    Datum d1,d2;
    char *emalloc();

    // Obtener variable
    d1=pop();
    // Obtener numero/cadena
    d2=pop();

    if (d1.sym->tipo != VAR && d1.sym->tipo != INDEFINIDA) {
        execerror(" Asignacion a un elemento que no es una variable ", d1.sym->nombre);
    }

    // Asignacion en funcion de subtipo
    if (d2.subtipo == CADENA) {
        // Reserva de memoria para cadena
        free(d1.sym->u.vchar);
        d1.sym->u.vchar = emalloc((strlen(d2.cadena)+1)*sizeof(char));

        // Copia cadena
        strcpy(d1.sym->u.vchar, d2.cadena);
    } else if (d2.subtipo == NUMBER) {
        // Asignar valor numerico
        d1.sym->u.val = d2.val;
    }

    // Asignacion de tipos
    d1.sym->tipo = VAR;
    d1.sym->subtipo = d2.subtipo;

    // Apilar variable
    push(d2);
}

void constpush() {
    // Introduce una constante en la pila
    Datum d;
    d.val = ((Symbol *)*pc++)->u.val;
    //d.tipo = CONSTANTE;
    d.subtipo = NUMBER;

    push(d);
}

void cadenapush() {
    // Introduce una cadena en la pila
    Datum d;
    char *emalloc();

    d.cadena = emalloc((strlen(((Symbol *)*pc)->u.vchar)+1)*sizeof(char));
    strcpy(d.cadena, ((Symbol *)*pc++)->u.vchar);
    d.subtipo = CADENA;

    push(d);
}

void cadena_a_minusculas(char *str) {
    int i;
    for (i = 0; str[i]; i++) {
        str[i] = tolower(str[i]);
    }

} // cadena_a_minusculas

char *get_extension_fichero(char *ruta_fichero) {
    // Buscar punto en el nombre de fichero
    char *punto = strrchr(ruta_fichero, '.');

    // Comprobar que se haya encontrado el punto
    if(!punto || punto == ruta_fichero) {
        return "";
    }

    // Devuelve extension del fichero
    return punto + 1;
} // get_extension_fichero

void dividir() {
    // dividir los dos valores superiores de la pila
    Datum d1,d2;

    // Obtener el primer numero
    d2=pop();
    // Obtener el segundo numero
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Division entre datos no numericos ", NULL);
    } else {
        // Tipos validos, dividir
        // Comprobar si hay division por 0
        if (d2.val == 0.0) {
            execerror (" Division por cero ", NULL);
        }
        d1.val = d1.val / d2.val;
    }

    // Apilar resultado
    push(d1);                    /* Apilar el resultado */
}

void division_entera() {
    // Divide la parte entera de los dos valores superiores de la pila
    Datum d1, d2;

    // Obtiene el primer numero
    d2 = pop();
    // Obtiene el segundo numero
    d1 = pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Division entera entre datos no numericos ", NULL);
    } else {
        // Tipos validos
        // Evitar division por 0
        if (d2.val == 0.0) {
            execerror (" Division por cero ", (char *) 0);
        }
        // Divide forzando el tipo entero (division de parte entera)
        d1.val = (int) d1.val / (int) d2.val;
    }

    // Apilar resultado
    push(d1);
} // division_entera

void escribir() {
    // sacar de la pila el valor superior y escribirlo como número
    Datum d;

    d=pop();  /* Obtener numero */

    // Comprobacion de tipos
    if (d.subtipo == NUMBER) {
        //printf("\t ---> %.8g\n",d.val);
        printf("%.8g",d.val);
    } else {
        // Tipo invalido
        execerror (" Dato no numerico ", NULL);
    }
}

void escribir_cadena() {
    // sacar de la pila el valor superior y escribirlo como cadena
    Datum d;

    // Obtener cadena
    d=pop();

    // Comprobacion de tipos
    if (d.subtipo == CADENA) {
        //printf("\t ---> %s\n", d.cadena);
        printf("%s", d.cadena);
    } else {
        // Tipo invalido
        execerror (" Dato no alfanumerico ", NULL);
    }
}

void eval() {
    /* evaluar una variable en la pila */
    Datum d;
    char *emalloc();

    // Obtener variable de la pila
    d=pop();

    /* Si la variable no esta definida */
    if (d.sym->tipo == INDEFINIDA) {
        execerror (" Variable no definida ", d.sym->nombre);
    }

    // Asignacion segun subtipos
    if (d.sym->subtipo == CADENA) {
        // Evaluacion de cadenas
        d.cadena = emalloc((strlen(d.sym->u.vchar)+1)*sizeof(char));
        strcpy(d.cadena, d.sym->u.vchar);
    } else if (d.sym->subtipo == NUMBER) {
        // Evaluacion de numeros
        d.val = d.sym->u.val;  /* Sustituir variable por valor */
    }

    // Apilar valor
    push(d);
}

void funcion0() {
    /* evaluar una funcion predefinida sin parametros */
    Datum d;

    d.val= (*(double (*)())(*pc++))();
    push(d);
}

void funcion1() {
    /* evaluar una funcion predefinida con un parametro */
    Datum d;

    d=pop();  /* Obtener parametro para la funcion */

    d.val= (*(double (*)())(*pc++))(d.val);
    push(d);
}

void funcion2() {
    /* evaluar una funcion predefinida con dos parametros */
    Datum d1,d2;

    d2=pop();  /* Obtener parametro para la funcion */
    d1=pop();  /* Obtener parametro para la funcion */

    d1.val= (*(double (*)())(*pc++))(d1.val,d2.val);
    push(d1);
}

void modulo() {
    /* resto de la division entera del segundo valor de la pila */
    /* por el valor de la cima */
    Datum d1,d2;

    // Obtener el divisor
    d2=pop();
    // Obtener el dividendo
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Modulo entre datos no numericos ", NULL);
    } else {
        // Tipos validos, calcular modulo
        // Comprobar si hay division por 0
        if (d2.val == 0.0) {
            execerror (" Division por cero ", NULL);
        }
        // Resto
        d1.val = (int) d1.val % (int) d2.val;
    }

    // Apilar el resultado
    push(d1);
}

void multiplicar() {
    // multiplicar los dos valores superiores de la pila */
    Datum d1,d2;

    // Obtener el primer numero
    d2=pop();
    // Obtener el segundo numero
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Producto entre datos no numericos ", NULL);
    } else {
        // Tipos validos, multiplicar
        d1.val = d1.val * d2.val;
    }

    // Apilar el resultado
    push(d1);
}

void negativo() {
    // negacion del valor superior de la pila
    Datum d1;

    // Obtener numero
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER) {
        execerror (" Negativo de dato no numerico ", NULL);
    } else {
        // Aplicar menos
        d1.val = - d1.val;
    }

    // Apilar resultado
    push(d1);
}

/* Esta funcion se puede omitir   */
void positivo() {
    /* tomar el valor positivo del elemento superior de la pila */
    Datum d1;

    // Obtener numero
    d1=pop();
    /* d1.val = + d1.val;*/     /* Aplicar mas    */
    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER) {
        execerror (" Positivo de dato no numerico ", NULL);
    }

    // Apilar resultado
    push(d1);
}

void potencia() {
    // Exponenciacion de los valores superiores de la pila
    Datum d1,d2;

    // Obtener exponente
    d2=pop();
    // Obtener base
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Potencia entre datos no numericos ", NULL);
    } else {
        // Tipos validos, exponenciar
        if ( (d1.val>=0) || ((int) d2.val == d2.val) ) {
            // Elevar a potencia
            d1.val = pow(d1.val,d2.val);
            // Apilar el resultado
            push(d1);
        } else {
            char digitos[20];
            sprintf(digitos,"%lf",d1.val);
            execerror(" radicando negativo ", digitos);
        }
    }

}

void restar() {
    /* restar los dos valores superiores de la pila */
    Datum d1,d2;

    // Obtener el primer numero
    d2=pop();
    // Obtener el segundo numero
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Resta entre datos no numericos ", NULL);
    } else {
        // Tipos validos, restar
        d1.val = d1.val - d2.val;
    }

    // Apilar el resultado
    push(d1);
}

void sumar() {
    /* sumar los dos valores superiores de la pila */
    Datum d1,d2;

    // Obtener el primer numero
    d2=pop();
    // Obtener el segundo numero
    d1=pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Suma entre datos no numericos ", NULL);
    } else {
        // Tipos validos, sumar
        d1.val = d1.val + d2.val;
    }

    // Apilar el resultado
    push(d1);
}

void concatenar() {
    // Concatena los dos valores superiores de la pila
    Datum d1, d2;
    char *emalloc();

    // Obtener la segunda cadena
    d2 = pop();
    // Obtener la primera cadena
    d1 = pop();

    // Comprobacion de validez de subtipos
    if (d1.subtipo != CADENA || d2.subtipo != CADENA) {
        execerror (" Concatenacion de datos no alfanumericos ", NULL);
    } else {
        // Tipos validos, concatenar
        d1.cadena = (char *) realloc(d1.cadena, (strlen(d1.cadena)+strlen(d2.cadena)+1)*sizeof(char));
        d1.cadena = strcat(d1.cadena, d2.cadena);
    }

    // Apilar valor
    push(d1);
}

void varpush() {
    /* meter una variable en la pila */
    Datum d;
    d.sym=(Symbol *)(*pc++);

    // Asignacion de tipos
    //d.tipo = d.sym->tipo;
    d.subtipo = d.sym->subtipo;

    // Apilar valor
    push(d);
}
/****************************************************************************/
/****************************************************************************/

void leervariable() {
    /* Leer una variable numerica por teclado */
    Symbol *variable;
    char c;

    variable = (Symbol *)(*pc); /* apunta a la variable que va a ser leida */

    /* Se comprueba si el identificador es una variable */
    if ((variable->tipo == INDEFINIDA) || (variable->tipo == VAR)) {
        //printf("Valor--> ");
        while((c=getchar())=='\n') ; /* elimina posibles saltos de linea existentes */
        ungetc(c,stdin);
        scanf("%lf",&variable->u.val); /* lee la variable y lo introduce en el vector de instrucciones */
        variable->tipo=VAR; /* actualiza su tipo como variable */
        variable->subtipo = NUMBER;
        pc++; /* desplaza el puntero a la posicion siguiente (localiza STOP en ejemplo) */
    } else {
        execerror("No es una variable",variable->nombre);
    }
}

void leer_cadena() {
    // Leer una variable alfanumerica por teclado
    Symbol *variable;
    char *emalloc(), c;
    int tam_buffer, i, j;

    variable = (Symbol *)(*pc); /* apunta a la variable que va a ser leida */

    /* Se comprueba si el identificador es una variable */
    if ((variable->tipo == INDEFINIDA) || (variable->tipo == VAR)) {
        //printf("Valor--> ");
        while((c=getchar())=='\n') ; /* elimina posibles saltos de linea existentes */
        ungetc(c,stdin);

        // Reserva de memoria para la nueva cadena
        tam_buffer = 1024*sizeof(char);
        variable->u.vchar = emalloc(tam_buffer);

        // Almacenamiento de cadena
        /* lee la variable y lo introduce en el vector de instrucciones */
        variable->u.vchar = fgets(variable->u.vchar, tam_buffer, stdin);
        variable->u.vchar[strlen(variable->u.vchar)-1] = '\0';

        // Procesamiento de cadena para escapar caracteres
        i = 0;
        j = 0;
        // Limpia comilla inicial
        if (variable->u.vchar[i] == '\'') {
            i = 1;
        }
        while(variable->u.vchar[i] != '\0') {
            if (variable->u.vchar[i] == '\\') {
                i++;

                if (variable->u.vchar[i] == 't' || variable->u.vchar[i] == 'n') {
                    // Introduce caracteres escapados
                    if (variable->u.vchar[i] == 'n') {
                        variable->u.vchar[j] = '\n';
                    }
                    if (variable->u.vchar[i] == 't') {
                        variable->u.vchar[j] = '\t';
                    }
                } else {
                    // Continua el desplazamiento
                    variable->u.vchar[j] = variable->u.vchar[i];
                }
            } else {
                // Continua el desplazamiento
                variable->u.vchar[j] = variable->u.vchar[i];
            }
            i++;
            j++;
        }
        // Elimina comilla final
        if (variable->u.vchar[j-1] == '\'') {
            j--;
        }
        variable->u.vchar[j] = '\0';

        // Ajuste de memoria al espacio realmente ocupado
        variable->u.vchar = (char *) realloc(variable->u.vchar, (strlen(variable->u.vchar)+1)*sizeof(char));

        // Actualizacion de tipos
        variable->tipo=VAR;
        variable->subtipo = CADENA;

        /* desplaza el puntero a la posicion siguiente (localiza STOP en ejemplo) */
        pc++;
    } else {
        execerror("No es una variable",variable->nombre);
    }
}

void mayor_que() {
    Datum d1,d2;

    // Obtener el segundo dato
    d2=pop();
    // Obtener el primer dato
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val > d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) > 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar el resultado
    push(d1);
}

void menor_que() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val < d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) < 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar el resultado
    push(d1);
}


void igual() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val == d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) == 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}

void mayor_igual() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val >= d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) >= 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}


void menor_igual() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val <= d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) <= 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}

void distinto() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    // Comparacion de validez de subtipos
    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Comparacion entre numericos
        if (d1.val != d2.val) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else if (d1.subtipo == CADENA && d2.subtipo == CADENA) {
        // Comparacion entre alfanumericos
        if (strcmp(d1.cadena, d2.cadena) != 0) {
            d1.val = 1;
        } else {
            d1.val = 0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Comparacion entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}


void y_logico() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Tipos validos, calcular AND
        if (d1.val==1 && d2.val==1) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Operacion AND entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}

void o_logico() {
    Datum d1,d2;

    // Obtener el segundo numero
    d2=pop();
    // Obtener el primer numero
    d1=pop();

    if (d1.subtipo == NUMBER && d2.subtipo == NUMBER) {
        // Tipos validos, calcular OR
        if (d1.val==1 || d2.val==1) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else {
        // Comparacion entre tipos invalidos, error
        execerror (" Operacion OR entre tipos incompatibles ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}

void negacion() {
    Datum d1;

    // Obtener numero
    d1=pop();

    if (d1.subtipo == NUMBER) {
        // Tipo valido, calcular NOT
        if (d1.val==0) {
            d1.val= 1;
        } else {
            d1.val=0;
        }
    } else {
        // Tipo invalido, error
        execerror (" Operacion NOT sobre dato no numerico ", NULL);
    }

    // Actualizar subtipo
    d1.subtipo = NUMBER;

    // Apilar resultado
    push(d1);
}


void whilecode() {
    Datum d;
    Inst *savepc = pc;    /* Puntero auxiliar para guardar pc */

    execute(savepc+2);    /* Ejecutar codigo de la condicion (esta a dos posiciones de distancia al almancenar dos STOPS temporales) */

    d=pop();    /* Obtener el resultado de la condicion de la pila */

    // Comprobacion de tipo valido
    if (d.subtipo != NUMBER) {
        execerror (" Condicion de bucle Mientras ... hacer no relacional o logica ", NULL);
    }

    while(d.val) {
        /* Mientras se cumpla la condicion del valor extraido */
        execute(*((Inst **)(savepc)));   /* Ejecutar codigo del cuerpo del bucle */
        execute(savepc+2);               /* Ejecutar condicion otra vez */
        d=pop();              /* Obtener el resultado de la condicion */
    }

    /* Asignar a pc la posicion del vector de instrucciones que contiene */
    /* la siguiente instruccion a ejecutar */
    pc= *((Inst **)(savepc+1)); /* continua por la siguiente instruccion */
}

void ifcode() {
    Datum d;
    Inst *savepc = pc;   /* Puntero auxiliar para guardar pc */

    execute(savepc+3);   /* Ejecutar condicion, que se conoce que esta a tres STOP de distancia => condicion del bucle */
    d=pop();             /* Obtener resultado de la condicion del bucle ejecutada */

    // Comprobacion de tipo valido
    if (d.subtipo != NUMBER) {
        execerror (" Condicion de condicional Si ... entonces no relacional o logica ", NULL);
    }

    /* Si se cumple la condición ejecutar el cuerpo del if */
    if (d.val) {
        execute(*((Inst **)(savepc)));
    } else if  (*((Inst **)(savepc+1))) {
        /* Si no se cumple la condicion se comprueba si existe parte else   */
        /* Esto se logra ya que la segunda posicion reservada contendria el */
        /* puntero a la primera instruccion del cuerpo del else en caso de  */
        /* existir, si no existe sera\A0 STOP, porque a la hora de generar    */
        /* codigo se inicializa con STOP.                                   */
        execute(*((Inst **)(savepc+1)));
    }

    /* Asignar a pc la posicion del vector de instrucciones que contiene */
    /* la siguiente instruccion a ejecutar */
    pc= *((Inst **)(savepc+2)); /* pasa a la instruccion siguiente */
}

void repetir_code() {
    Datum d;
    Inst *savepc = pc;    /* Puntero auxiliar para guardar pc */

    do {
        // Repetir mientras no se cumpla la condicion indicada
        // Ejecutar codigo del cuerpo del bucle, que esta a 2 STOP de distancia
        execute(savepc+2);
        // Ejecutar condicion
        execute(*((Inst **)(savepc)));
        // Obtener el resultado de la condicion
        d = pop();

        // Comprobacion de tipo de condicion valida
        if (d.subtipo != NUMBER) {
            execerror (" Condicion de bucle Repetir ... hasta no relacional o logica ", NULL);
        }
    } while(!d.val);

    /* Asignar a pc la posicion del vector de instrucciones que contiene */
    /* la siguiente instruccion a ejecutar */
    pc= *((Inst **)(savepc+1)); /* continua por la siguiente instruccion */
}

void para_code() {
    // Expresiones numericas del bucle
    Datum expr_desde,
          expr_hasta,
          expr_paso;
    // Variable del bucle
    Symbol *var;
    // Puntero auxiliar para guardar pc
    Inst *savepc = pc;

    // Extraccion de variable
    var = *((Symbol **)(savepc+5));
    var->tipo = VAR;
    var->subtipo = NUMBER;

    // Extraccion de desde
    execute(*((Inst **)(savepc)));
    expr_desde = pop();
    if (expr_desde.subtipo != NUMBER) {
        execerror (" Parametro Desde de bucle Para ... hacer no relacional o logica ", NULL);
    }

    // Expresion de hasta
    execute(*((Inst **)(savepc+1)));
    expr_hasta = pop();
    if (expr_hasta.subtipo != NUMBER) {
        execerror (" Parametro Hasta de bucle Para ... hacer no relacional o logica ", NULL);
    }

    // Expresion de paso
    execute(*((Inst **)(savepc+2)));
    expr_paso = pop();
    if (expr_paso.subtipo != NUMBER) {
        execerror (" Parametro Paso de bucle Para ... hacer no relacional o logica ", NULL);
    }

    /* Comprobacion de rango de bucle valido
    * Un bucle for puede ser infinito cuando:
    * - Debe alcanzarse un valor mayor al inicial y el paso es negativo
    * - Debe alcanzarse un valor menor al inicial y el paso es positivo
    * - El paso es nulo
    */
    if (( (expr_desde.val < expr_hasta.val) && (expr_paso.val < 0) )
        || ( (expr_desde.val > expr_hasta.val) && (expr_paso.val > 0))
        || (expr_paso.val == 0)) {
        execerror (" Parametros de bucle Para ... hacer generan bucle infinito ", NULL);
    }

    // Ejecucion de bucle para segun valor del paso
    if (expr_paso.val > 0) {
        for (var->u.val = expr_desde.val; var->u.val <= expr_hasta.val; (var->u.val) += (expr_paso.val)) {
            // Ejecucion de cuerpo de bucle
            execute(*((Inst **)(savepc+3)));

        }
    } else {
        for (var->u.val = expr_desde.val; var->u.val >= expr_hasta.val; (var->u.val) += (expr_paso.val)) {
            // Ejecucion de cuerpo de bucle
            execute(*((Inst **)(savepc+3)));

        }
    }

    /* Asignar a pc la posicion del vector de instrucciones que contiene */
    /* la siguiente instruccion a ejecutar */
    pc= *((Inst **)(savepc+4)); /* continua por la siguiente instruccion */
}

/****************************************************************************/
/****************************************************************************/
void lugar() {
    /* Situa el cursor en una coordenada determinada de la pantalla */
    Datum d1,d2;

    // Obtiene coordenada Y
    d2=pop();
    // Obtiene coordenada X
    d1=pop();

    if (d1.subtipo != NUMBER || d2.subtipo != NUMBER) {
        execerror (" Parametros de _lugar no numericos ", NULL);
    } else {
        // Tipos validos, situa cursor en coordenadas
        LUGAR((int) d1.val, (int) d2.val);
    }

}

void borrar() {
    // Elimina el contenido de la terminal de comandos
    CLEAR_SCREEN;
}
