/* Activa insensibilidad a mayusculas */
%option caseless

%{
#include "macros.h"
#include "ipe.h"
#include "ipe.tab.h"

extern char *progname;
extern int lineno;

%}

/* definiciones regulares */
/* Palabras reservadas */
_mod            _(?i:mod)
_div            _(?i:div)
_o              _(?i:o)
_y              _(?i:y)
_no             _(?i:no)
_borrar         _(?i:borrar)
_lugar          _(?:lugar)

/* Identificadores, cadenas, numeros */
letra          [a-zA-Z]
digito         [0-9]
numero         {digito}+(\.{digito}+)?((?i:e)[+\-]?{digito}+)?
caracter       [a-zA-Z0-9]
identificador  {letra}({caracter}|(_{caracter}))*{caracter}*

/* ESTADOS */
%x ESTADO_CADENA
%x ESTADO_COMENTARIO
%x ESTADO_COMENTARIO_MULTILINEA

%%
[ \t] { ; } /* saltar los espacios y los tabuladores */


{_mod} { return MODULO; }

{_div} { return DIVISION_ENTERA; }

{_o} { return O_LOGICO; }

{_y} { return Y_LOGICO; }

{_no} { return NEGACION; }

{_borrar} { return _BORRAR; }

{_lugar} { return _LUGAR; }


{numero} {
    double d;
    sscanf(yytext,"%lf",&d);
    /* Instala el numero en la tabla de simbolos */
    yylval.sym=install("", NUMBER, d);
    return NUMBER;
}


"'" {
    // Comienzo de una cadena de texto
    BEGIN ESTADO_CADENA;
}
<ESTADO_CADENA>"'" {
    // Fin de cadena
    int i, j;

    // Retorna al estado inicial
    BEGIN(INITIAL);

    // Elimina la ultima '
    yytext[yyleng - 1] = '\0';

    // Inicializacion de memoria para cadena
    char *cadena = (char *) calloc(yyleng+1, sizeof(char));
    if (cadena == NULL) {
        execerror(" Error al reservar memoria de cadena. ", (char *) 0);
    }
    strcpy(cadena, yytext);

    // Procesamiento de cadena para escapar caracteres
    i = 0;
    j = 0;

    while(cadena[i] != '\0') {
        if (cadena[i] == '\\') {
            i++;

            if (cadena[i] == 't' || cadena[i] == 'n') {
                // Introduce caracteres escapados
                if (cadena[i] == 'n') {
                    cadena[j] = '\n';
                }
                if (cadena[i] == 't') {
                    cadena[j] = '\t';
                }
            } else {
                // Continua el desplazamiento
                cadena[j] = cadena[i];
            }
        } else {
            // Continua el desplazamiento
            cadena[j] = cadena[i];
        }
        i++;
        j++;
    }
    cadena[j] = '\0';

    // Instala la cadena en la tabla de simbolos
    yylval.sym=install_cadena("", CADENA, cadena);
    free(cadena);
    return CADENA;
}
<ESTADO_CADENA><<EOF>> {
    // Cadena sin cerrar
    execerror(" Cadena sin cerrar ", NULL);

    // Finaliza el analisis
    yyterminate();
}
<ESTADO_CADENA>"\\\'" {
    // Permite la introduccion de ' dentro de la cadena
    yymore();
}
<ESTADO_CADENA>\n {
    // Continua la lectura de la cadena tras salto de linea
    yymore();
    lineno++;
}
<ESTADO_CADENA>\t { yymore(); }
<ESTADO_CADENA>. { yymore(); }

"@" {
    // Comentario de una linea
    BEGIN(ESTADO_COMENTARIO);
}
<ESTADO_COMENTARIO>. {
    // Cuerpo del comentario de una linea
    ;
}
<ESTADO_COMENTARIO>\n {
    // Fin del comentario de una linea
    BEGIN(INITIAL);
    lineno++;
}


"#" {
    // Inicio de comentario de multiples lineas
    BEGIN(ESTADO_COMENTARIO_MULTILINEA);
}
<ESTADO_COMENTARIO_MULTILINEA>"#" {
    // Fin del comentario de multiples lineas
    BEGIN(INITIAL);
}
<ESTADO_COMENTARIO_MULTILINEA>. {
    // Continua leyendo el comentario de multiples lineas
    yymore();
}
<ESTADO_COMENTARIO_MULTILINEA>\n {
    // Continua leyendo el comentario de multiples lineas
    yymore();
    lineno++;
}
<ESTADO_COMENTARIO_MULTILINEA><<EOF>> {
    // Comentario sin cerrar
    execerror(" Comentario de varias lineas sin cerrar. ", NULL);

    // Finaliza el analisis
    yyterminate();
}

{identificador} {
    Symbol *s;

    /* Conversion de la cadena a minusculas */
    cadena_a_minusculas(yytext);

    if ((s=lookup(yytext)) == 0) {
        s = install(yytext, INDEFINIDA, 0.0);
    }
    yylval.sym = s;
    return s->tipo == INDEFINIDA ? VAR : s->tipo;
}



"||"    { return CONCATENACION; }
"**"    { return POTENCIA; }
">="    { return MAYOR_IGUAL; }
"<="    { return MENOR_IGUAL; }
"="     { return IGUAL; }
"<>"    { return DISTINTO; }
">"     { return MAYOR_QUE; }
"<"     { return MENOR_QUE; }
":="    { return ASIGNACION; }

\n      { lineno++; } /* no se devuelve nada, contin�a el analisis l�xico */
.       { return yytext[0]; }

