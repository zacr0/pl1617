/*
  Interprete de pseudocodigo en castellano:

    Se ha ampliado el ejemplo 9 para permitir:
      - operador de concatenacion.
      - operador de division entera.
      - operadores relacionales y l�gicos
      - sentencia condicional: si
      - sentencia iterativa: mientras
      - sentencia iterativa: repetir hasta
      - sentencia iterativa: para desde hasta paso hacer
      - ejecutar las sentencias contenidas en un fichero indicado en la l�nea de comandos
      - control de los ficheros indicados en la l�nea de comandos.

      Por ejemplo:
        > ./ipe.exe ejemplos/ejemplo_1_saluda.e
*/

%{
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "ipe.h"

#include "macros.h"

#define code2(c1,c2)         code(c1); code(c2)
#define code3(c1,c2,c3)      code(c1); code(c2); code(c3)
#define code6(c1,c2,c3,c4,c5,c6)    code(c1); code(c2); code(c3); code(c4); code(c5); code(c6);
    
%}

%union {
    Symbol *sym;    /* puntero a la tabla de simbolos */
    Inst *inst;     /* instruccion de maquina */
}

%token <sym> NUMBER VAR CADENA CONSTANTE FUNCION0_PREDEFINIDA FUNCION1_PREDEFINIDA FUNCION2_PREDEFINIDA INDEFINIDA PRINT PRINT_CADENA WHILE HACER FIN_MIENTRAS REPETIR HASTA PARA DESDE PASO FIN_PARA IF ENTONCES ELSE FIN_SI READ READ_CADENA _LUGAR _BORRAR
%type <inst> stmt asgn expr stmtlist cond while if repetir var para end
%right ASIGNACION
%left O_LOGICO
%left Y_LOGICO
%left MAYOR_QUE MENOR_QUE MENOR_IGUAL MAYOR_IGUAL DISTINTO IGUAL
%left '+' '-'
%left '*' '/' MODULO DIVISION_ENTERA
%left UNARIO NEGACION
%left CONCATENACION
%right POTENCIA
%%

list:    /* nada: epsilon produccion */
    | list stmt  ';' {
        code(STOP);
        return 1;
    }
    | list error ';' { yyerrok; };

stmt:    /* nada: epsilon produccion */  { $$=progp; }
    | asgn          { code(pop2); }
    | _BORRAR               {code(borrar);}
    | PRINT '(' expr ')'   { code(escribir); $$ = $3; }
    | PRINT_CADENA '(' expr ')'   { code(escribir_cadena); $$ = $3; }
    | READ '(' VAR ')'    { code2(leervariable,(Inst)$3); } /* introduce leervariable y la variable en la tabla de instrucciones */
    | READ_CADENA '(' VAR ')'    { code2(leer_cadena,(Inst)$3); } /* introduce leercadena y la variable en la tabla de instrucciones */
    | _LUGAR '(' expr ',' expr ')' { code(lugar); }
    | while cond HACER stmtlist FIN_MIENTRAS end {
        /* establece los enlaces del cuerpo y la siguiente instruccion */
        /* posicion del primer stop */
        ($1)[1]=(Inst)$4; /* cuerpo del bucle */
        /* posicion del segundo stop */
        ($1)[2]=(Inst)$6; /* siguiente instruccion al bucle */
    }
    | if cond ENTONCES stmtlist FIN_SI end {
        /* proposicion if sin parte else */
        ($1)[1]=(Inst)$4; /* cuerpo del if */
        ($1)[3]=(Inst)$6; /* siguiente instruccion al if */
    }
    | if cond ENTONCES stmtlist end ELSE stmtlist FIN_SI end {
        /* proposicion if con parte else */
        /* posicion del primer stop */
        ($1)[1]=(Inst)$4; /* cuerpo del if, apunta a cuerpo del consecuente */
        /* posicion del segundo stop */
        ($1)[2]=(Inst)$7; /* cuerpo del else, apunta a alternativa */
        /* posicon del tercer STOP */
        ($1)[3]=(Inst)$9; /* siguiente instruccion al if-else, apunta a siguiente instruccion */
    }
    | repetir stmtlist end HASTA cond end {
        // Bucle repetir sentencias hasta condicion
        // Se establecen los enlaces a la condicion y a la siguiente instruccion
        ($1)[1] = (Inst)$5; // condicion del bucle
        ($1)[2] = (Inst)$6; // siguiente instruccion al bucle
    }
    | para var DESDE expr end HASTA expr end PASO expr end HACER stmtlist FIN_PARA end {
        /*
        * Bucle para identificador desde expresion numerica1 hasta
        * expresion numerica 2 paso expresion numerica 3 hacer
        * sentencias fin_para
        *
        * Se establecen los enlaces a la expresion1, expresion2, expresion3
        * cuerpo y siguiente instruccion
        */
        ($1)[1] = (Inst)$4;  // expresion1 (desde)
        ($1)[2] = (Inst)$7;  // expresion2 (hasta)
        ($1)[3] = (Inst)$10; // expresion3 (paso)
        ($1)[4] = (Inst)$13; // cuerpo del bucle (hacer)
        ($1)[5] = (Inst)$15; // siguiente instruccion al bucle
    }


asgn: VAR ASIGNACION expr { $$=$3; code3(varpush,(Inst)$1,assign);}
    | CONSTANTE ASIGNACION expr {
        execerror(" NO se pueden asignar datos a constantes ",$1->nombre);
    }
    | NUMBER ASIGNACION expr {
        execerror(" NO se pueden asignar datos a numeros ", NULL);
    }
    | CADENA ASIGNACION expr {
        execerror(" NO se pueden asignar datos a cadenas ", NULL);
    };

cond:    '(' expr ')' {code(STOP); $$ =$2;} /* introduce stop y la condicion en la pila de instrucciones */
    ;

while: WHILE { $$=code3(whilecode,STOP,STOP); } /* introduce whilecode y dos STOPS de separacion en la pila de instrucciones */
    ;

if: IF {
    /* almacena ifcode y tres stops (consecuente, alternativa, instruccion siguiente) en la pila de instrucciones */
    $$=code(ifcode);
    code3(STOP,STOP,STOP);
};

repetir: REPETIR {
    // Almacena repetir_code e introduce dos stops (cuerpo e instruccion siguiente) en la pila de instrucciones
    $$=code3(repetir_code, STOP, STOP);
}

var: VAR {
    // Variable para el bucle para
    code((Inst)$1); // primer argumento (la variable)
    $$ = progp;
}

para: PARA {
    /*
    * Almacena para_code e introduce cinco stops (expresion1, expresion2,
    * expresion3, cuerpo e instruccion siguiente) en la pila de instrucciones
    */
    $$ = code6(para_code, STOP, STOP, STOP, STOP, STOP);
}

end :    /* nada: produccion epsilon */  {code(STOP); $$ = progp;} /* delimitador */
        ;

stmtlist:  /* nada: produccion epsilon */ {$$=progp;}
    | stmtlist stmt ';';


expr : NUMBER     		{$$=code2(constpush,(Inst)$1);}
    | CADENA            {$$=code2(cadenapush,(Inst)$1);}
    | VAR               {$$=code3(varpush,(Inst)$1,eval);} /* introduce varpush, dato (variable) y eval en la pila de instrucciones */
    | CONSTANTE      	{$$=code3(varpush,(Inst)$1,eval);}
    | asgn
    | FUNCION0_PREDEFINIDA '(' ')'      {code2(funcion0,(Inst)$1->u.ptr);}
    | FUNCION1_PREDEFINIDA '(' expr ')' {$$=$3;code2(funcion1,(Inst)$1->u.ptr);}
    | FUNCION2_PREDEFINIDA '(' expr ',' expr ')'
        {$$=$3;code2(funcion2,(Inst)$1->u.ptr);}
    | '(' expr ')'  	{$$ = $2;}
    | expr '+' expr 	{code(sumar);}
    | expr '-' expr 	{code(restar);}
    | expr '*' expr 	{code(multiplicar);}
    | expr '/' expr 	{code(dividir);}
    | expr DIVISION_ENTERA expr {code(division_entera);}
    | expr MODULO expr 	{code(modulo);}
    | expr POTENCIA expr 	{code(potencia);}
    |'-' expr %prec UNARIO 	{$$=$2; code(negativo);}
    |'+' expr %prec UNARIO 	{$$=$2; code(positivo);}
    | expr CONCATENACION expr   {code(concatenar);}
    | expr MAYOR_QUE expr 	{code(mayor_que);}
    | expr MAYOR_IGUAL expr {code(mayor_igual);}
    | expr MENOR_QUE expr 	{code(menor_que);}
    | expr MENOR_IGUAL expr {code(menor_igual);}
    | expr IGUAL expr 	    {code(igual);}
    | expr DISTINTO expr 	{code(distinto);}
    | expr Y_LOGICO expr 	{code(y_logico);}
    | expr O_LOGICO expr 	{code(o_logico);}
    | NEGACION expr 	{$$=$2; code(negacion);};

%%

#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <setjmp.h>

/* Depuracion
     1: habilitada
     0: no habilitada

     Se debe usar
      bison --debug ...
    o
      bison -t
*/
int yydebug=0;

/* Nombre del programa */
char *progname;

/* Contador de l�neas */
int lineno = 1;

/* Permite la recuperaci�n de errores */
jmp_buf begin;

/* Dispositivo de entrada est�ndar de yylex() */
extern FILE * yyin;


int main(int argc, char *argv[]) {
    // Fichero de pseudocodigo externo
    FILE *fich = NULL;
    char *ruta_fichero = NULL;

    /* Si se invoca el int�rprete con un fichero de entrada */
    /* entonces se establece como dispositivo de entrada para yylex() */
    if (argc == 2) {
        ruta_fichero = argv[1];

        if ((fich = fopen(ruta_fichero, "r")) != NULL) {
            // El fichero existe

            // Comprobar validez de extension de fichero
            cadena_a_minusculas(ruta_fichero);
            if (strcmp(get_extension_fichero(ruta_fichero), "e") == 0) {
                // Extension de fichero valida
                yyin = fich;
            } else {
                // Extension invalida
                fprintf(stderr, "ERROR: El fichero \'%s\' debe tener la extension \'.e\'.\n", argv[1]);
                return -1;
            }
        } else {
            // El fichero no existe
            fprintf(stderr, "ERROR: El fichero \'%s\' no existe.\n", ruta_fichero);
            return -1;
        }
    }

    progname=argv[0];

    /* inicializacion de la tabla de simbolos */
    init();

    /* Establece un estado viable para continuar despues de un error */
    setjmp(begin);

    /* Establece cual va a ser la funcion para tratar errores de punto flotante */
    signal(SIGFPE,fpecatch); /* Excepcion de punto flotante*/

    /* initcode inicializa el vector de intrucciones y la pila del interprete */
    for (initcode(); yyparse(); initcode()) {
        execute(prog);
    }

    // Cierre de fichero de datos
    fclose(fich);

    return 0;
}

void yyerror(char *s) {
    warning(s,(char *) 0);
}

void warning(char *s, char *t) {
    fprintf(stderr," ** %s : %s", progname,s);
    if (t) {
        fprintf(stderr," ---> %s ",t);
    }
    fprintf(stderr,"  (linea %d)\n",lineno);
}

void execerror(s,t) /* recuperacion de errores durante la ejecucion */
char *s,*t;
{
    warning(s,t);
    longjmp(begin,0);
}

void fpecatch() {
    /*  atrapa errores de coma flotante */
    execerror("error de coma flotante ",(char *)0);
}
