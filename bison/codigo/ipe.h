/*!
	\file ipe.h
	\brief Prototipos de funciones auxiliares
*/

#ifndef IPE_H
#define IPE_H

/* Analizador léxico */
int yylex();

/* Informa de la detección de un error sintáctico */
void yyerror(char *s);

/* Recuperacion de errores durante la ejecucion */
void execerror(char *s,char *t);

/*  Escribe por pantalla información sobre un error sintáctico */
void warning(char *s, char *t);

/* Captura de errores de punto flotante */
void fpecatch();

/* Inicializa la tabla de símbolos */
void init();

typedef struct Symbol {
	char *nombre;  /* nombre del token */
	short tipo;    /* VAR, CONSTANTE, FUNCION, INDEFINIDA */
	short subtipo; /* Subtipo: NUMBER, CADENA */

	struct {
		double val;        /* si es VAR: para el subtipo NUMBER*/
		char *vchar;	   /* valor alfanumerico: para el subtipo STRING */
		double (*ptr)();   /* si es FUNCION */
	} u;

	struct Symbol * siguiente;
} Symbol;


/* Instala en la tabla de símbolos */
Symbol *install(char *s, int t, double d);
Symbol *install_cadena(char *s, int t, char *cadena);

/* Busca en la tabla de símbolos */
Symbol *lookup(char *s);

// NOTA: Cambiada deficion original de union a struct
typedef struct Datum {
	// tipo de la pila del interprete
	short tipo;    /* VAR, CONSTANTE, FUNCION, INDEFINIDA */
	short subtipo; /* Subtipo: NUMBER, CADENA */
	double val;
	char *cadena;
	Symbol *sym;
} Datum;


/* Función que introduce un dato de la pila de valores */
void push(Datum d);

/* Funciones que sacan un dato de la pila de valores */
extern Datum pop();
extern void pop2();

typedef void (*Inst)(); /* instruccion maquina */
#define STOP (Inst) 0

/* Vector de instrucciones */
extern Inst prog[];

/* NOVEDAD */
extern Inst* progp;

/* Función que inicializa el vector de instrucciones */
void initcode();

/* Función que inserta una instucción en el vector de instrucciones */
Inst *code(Inst f);

/* Función que ejecuta una función del vector de instrucciones */
void execute(Inst *p);

/* Prototipos de funciones auxiliares */
extern void assign();
extern void constpush();
extern void cadenapush();

void cadena_a_minusculas(char *str);
char *get_extension_fichero(char *ruta_fichero);

void dividir();
void division_entera();
void escribir();
void escribir_cadena();
void eval();

void funcion0();
void funcion1();
void funcion2();

void modulo();
void multiplicar();
void negativo();
void positivo();
void potencia();
void restar();
void sumar();
void concatenar();
void varpush();

void ifcode();
void whilecode();
void repetir_code();
void para_code();

void mayor_que();
void menor_que();
void mayor_igual();
void menor_igual();
void igual();
void distinto();
void y_logico();
void o_logico();
void negacion();

void leervariable();
void leer_cadena();

void lugar();
void borrar();

#endif
