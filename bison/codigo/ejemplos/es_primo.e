#
  Asignatura:    Procesadores de Lenguajes

  Titulación:    Ingeniería Informática
  Especialidad:  Computación
  Curso:         Tercero
  Cuatrimestre:  Segundo

  Departamento:  Informática y Análisis Numérico
  Centro:        Escuela Politécnica Superior de Córdoba
  Universidad de Córdoba

  Curso académico: 2016 - 2017

  Fichero de ejemplo para el intérprete de pseudocódigo en español: ipe.exe
#
@ Comprobacion de numero primo
_borrar;
_lugar(10,10);
escribir_cadena('Comprobar si un numero es primo');
_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena( pausa);

_borrar;
_lugar(10,10);
escribir_cadena('- Introduzca el numero a comprobar: ');
leer(num);

@ Comprobacion de numero mediante su resto
x := 2;
resto := num _mod x;
mientras((resto <> 0) _y (x < num)) hacer
	x := x+1;
	resto := num _mod x;
fin_mientras;

@ Muestra resultados
_borrar;
_lugar(10,10);

si (x = num) entonces
	escribir_cadena('- El numero \'');
	escribir(num);
	escribir_cadena('\' es primo.\n');
si_no
	escribir_cadena('- El numero \'');
	escribir(num);
	escribir_cadena('\' NO es primo.\n');
fin_si;

_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena( pausa);

@ Despedida
_borrar;
_lugar(10,10);
escribir_cadena('El programa ha concluido.\n');
