#
  Asignatura:    Procesadores de Lenguajes

  Titulación:    Ingeniería Informática
  Especialidad:  Computación
  Curso:         Tercero
  Cuatrimestre:  Segundo

  Departamento:  Informática y Análisis Numérico
  Centro:        Escuela Politécnica Superior de Córdoba
  Universidad de Córdoba

  Curso académico: 2016 - 2017

  Fichero de ejemplo para el intérprete de pseudocódigo en español: ipe.exe
#
@ Bienvenida

_borrar;

_lugar(10,10);

escribir_cadena('Introduce tu nombre --> ');

leer_cadena(nombre);

_borrar;
_lugar(10,10);

escribir_cadena(' Bienvenido/a << ');

escribir_cadena(nombre);

escribir_cadena(' >> al intérprete de pseudocódigo en español:\'ipe.exe\'.');

_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena(pausa);


repetir 

 @ Opciones disponibles

 _borrar;

 _lugar(10,10);
 escribir_cadena(' Determinar si un numero es primo --> 1 ');

 _lugar(11,10);
 escribir_cadena(' Separar numero en sus digitos ----> 2 ');

 _lugar(12,10);
 escribir_cadena(' Finalizar ---------------> 0 ');

 _lugar(15,10);
 escribir_cadena(' Elige una opcion: ');

 leer(opcion);

 _borrar;

 si (opcion  = 0)        @ Fin del programa
    entonces  
        _lugar(10,10);
        escribir_cadena(nombre);
        escribir_cadena(': gracias por usar el intérprete ipe.exe ');

   si_no
       @ Determinar si un numero es primo
	si (opcion = 1)
   	    entonces
	        _borrar;
			_lugar(10,10);
			escribir_cadena('Comprobar si un numero es primo');
			_lugar(40,10);
			escribir_cadena('Pulsa una tecla para continuar: ');
			leer_cadena( pausa);

			_borrar;
			_lugar(10,10);
			escribir_cadena('- Introduzca el numero a comprobar: ');
			leer(num);

			@ Comprobacion de numero mediante su resto
			x := 2;
			resto := num _mod x;
			mientras((resto <> 0) _y (x < num)) hacer
				x := x+1;
				resto := num _mod x;
			fin_mientras;

			@ Muestra resultados
			_borrar;
			_lugar(10,10);

			si (x = num) entonces
				escribir_cadena('- El numero \'');
				escribir(num);
				escribir_cadena('\' es primo.\n');
			si_no
				escribir_cadena('- El numero \'');
				escribir(num);
				escribir_cadena('\' NO es primo.\n');
			fin_si;
    
   	@ Separar numero en sus digitos
	si_no 
		si (opcion = 2)
			entonces
				_borrar;
				_lugar(10,10);
				escribir_cadena('Separar un numero entero en sus digitos.');
				_lugar(40,10);
				escribir_cadena('Pulsa una tecla para continuar: ');
				leer_cadena( pausa);

				@n := -1;
				repetir
				    _borrar;
				    _lugar(10,10);
				    escribir_cadena('- Introduzca un numero entero positivo: ');
				    leer(n);

				    @ Comprueba validez de numero
				    si (n < 0) entonces
				        @ Numero introducido invalido
				        _borrar;
				        _lugar(10,10);
				        escribir_cadena('- Debe introducir un numero positivo.');

				        _lugar(40,10);
				        escribir_cadena('Pulsa una tecla para continuar:');
				        leer_cadena( pausa);
				    fin_si;
				hasta(n >= 0);


				@ Contar digitos
				cont := 0;
				aux := n;
				Mientras (aux>0) hacer
				    @ mientras no sea cero
				    cont := cont + 1;
				    aux := aux _div 10;
				fin_mientras;

				@ Mostrar conteo
				_borrar;
				_lugar(10,10);
				escribir_cadena(' - El numero tiene ');
				escribir(cont);
				escribir_cadena(' digitos.\n');

				_lugar(40,10);
				escribir_cadena('Pulsa una tecla para continuar: ');
				leer_cadena( pausa);
				_borrar;

				@ Mostrar los digitos uno a uno
				aux := n;
				Para i desde 1 hasta cont paso 1 Hacer
				    pot := 10**(cont-i);

				    @ Obtener digito
				    digito := aux _div pot;

				    @ Quitar ese digito al numero
				    aux := aux - digito*pot;

				    @ Mostrar digito
				    _lugar(7+i,10);
				    escribir_cadena('- El digito en la posicion ');
				    escribir(i);
				    escribir_cadena(' es \'');
				    escribir(digito);
				    escribir_cadena('\'.');
				fin_para;

			@ Resto de opciones
 			si_no  
				_lugar(15,10);
			    	escribir_cadena(' Opcion incorrecta ');

 			fin_si;   
  	fin_si;                 

  fin_si;                          

 _lugar(40,10); 
 escribir_cadena('\n Pulse una tecla para continuar: ');
 leer_cadena(pausa);
 
hasta (opcion = 0);             

@ Despedida final

_borrar;
_lugar(10,10);
escribir_cadena('El programa ha concluido');
