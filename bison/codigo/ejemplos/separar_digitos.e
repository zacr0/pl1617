#
  Asignatura:    Procesadores de Lenguajes

  Titulación:    Ingeniería Informática
  Especialidad:  Computación
  Curso:         Tercero
  Cuatrimestre:  Segundo

  Departamento:  Informática y Análisis Numérico
  Centro:        Escuela Politécnica Superior de Córdoba
  Universidad de Córdoba

  Curso académico: 2016 - 2017

  Fichero de ejemplo para el intérprete de pseudocódigo en español: ipe.exe
#
@ Separa un numero entero en sus digitos
_borrar;
_lugar(10,10);
escribir_cadena('Separar un numero entero en sus digitos.');
_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena( pausa);

@n := -1;
repetir
    _borrar;
    _lugar(10,10);
    escribir_cadena('- Introduzca un numero entero positivo: ');
    leer(n);

    @ Comprueba validez de numero
    si (n < 0) entonces
        @ Numero introducido invalido
        _borrar;
        _lugar(10,10);
        escribir_cadena('- Debe introducir un numero positivo.');

        _lugar(40,10);
        escribir_cadena('Pulsa una tecla para continuar.');
        leer_cadena( pausa);
    fin_si;
hasta(n >= 0);


@ Contar digitos
cont := 0;
aux := n;
Mientras (aux>0) hacer
    @ mientras no sea cero
    cont := cont + 1;
    aux := aux _div 10;
fin_mientras;

@ Mostrar conteo
_borrar;
_lugar(10,10);
escribir_cadena(' - El numero tiene ');
escribir(cont);
escribir_cadena(' digitos.\n');

_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena( pausa);
_borrar;

@ Mostrar los digitos uno a uno
aux := n;
Para i desde 1 hasta cont paso 1 Hacer
    pot := 10**(cont-i);

    @ Obtener digito
    digito := aux _div pot;

    @ Quitar ese digito al numero
    aux := aux - digito*pot;

    @ Mostrar digito
    _lugar(7+i,10);
    escribir_cadena('- El digito en la posicion ');
    escribir(i);
    escribir_cadena(' es \'');
    escribir(digito);
    escribir_cadena('\'.');
fin_para;

_lugar(40,10);
escribir_cadena('Pulsa una tecla para continuar: ');
leer_cadena( pausa);

@ Despedida
_borrar;
_lugar(10,10);
escribir_cadena('El programa ha concluido.\n');
